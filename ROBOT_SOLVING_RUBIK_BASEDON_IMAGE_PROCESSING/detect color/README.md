
## Getting Started


### Step 1 - Install Dependencies


1. Install packages

    ```python
    pip3 install numpy
    pip install python3-opencv
    
    ```
    

### Step 2 - Find Threshold
```python
 python3 getThreshold.py
```
## Step 3 -Run
```python
 python3 main.py
```
## Authors
-Che Quang Huy - Develope and Operation - [chequanghuy](https://github.com/chequanghuy)
