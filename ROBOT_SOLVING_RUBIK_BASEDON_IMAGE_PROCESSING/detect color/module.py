import numpy as np
min1 = np.array([77,11,189],np.uint8)       
max1 = np.array([100,255,255],np.uint8)  #while

min2 = np.array([0,164,196 ],np.uint8)       
max2 = np.array([26,255,255],np.uint8) #orange

min3 = np.array([47,139,93 ],np.uint8)       
max3 = np.array([93,255,222],np.uint8)   #green

min4 = np.array([142,136,51  ],np.uint8)       
max4 = np.array([180,255,241],np.uint8)   # red

min5 = np.array([79,117,90  ],np.uint8)       
max5 = np.array([146,255,222],np.uint8)   #blue

min6 = np.array([19,199,105 ],np.uint8)       
max6 = np.array([52,255,255],np.uint8)   # yellow

min=[]
min.append(min1)
min.append(min2)
min.append(min3)
min.append(min4)
min.append(min5)
min.append(min6)

max=[]
max.append(max1)
max.append(max2)
max.append(max3)
max.append(max4)
max.append(max5)
max.append(max6)

colorChar=['White','Orange','Green','Red','Blue','Yellow']

def sort():
    tmp = np.zeros((1, 3),dtype = 'int')
    swapped = 1
    j = 0
    while swapped == 1:
        swapped = 0
        j=j+1
        for k in range(0,9-j):
            if a[k][1] > a[k + 1][1]:
        
                tmp[0] = a[k]
                a[k] = a[k + 1]
                a[k + 1] = tmp
                swapped = 1
    count=0
    while count<3:
        swapped = 1
        j = 0
        while swapped == 1:
            swapped = 0
            j=j+1
            for k in range(0,3-j):
                if a[k+count*3][0] > a[k + 1+count*3][0]:            
                    tmp[0] = a[k+count*3]
                    a[k+count*3] = a[k + 1+count*3]
                    a[k + 1+count*3] = tmp
                    swapped = 1
        
        count=count+1